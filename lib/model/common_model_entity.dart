import 'package:scoped_model/scoped_model.dart';

class CommonModelEntity extends Model {
  String uname;
  String mobile;
  String avatar;
  bool isLogin = false;

  CommonModelEntity({this.uname, this.mobile, this.avatar});

  CommonModelEntity.fromJson(Map<String, dynamic> json) {
    uname = json['uname'];
    mobile = json['mobile'];
    avatar = json['avatar'];
    isLogin = json['uname'] != null ? true : false;
  }

  fromJson(Map<String, dynamic> json) {
    uname = json['uname'];
    mobile = json['mobile'];
    avatar = json['avatar'];
    isLogin = json['uname'] != null ? true : false;
    notifyListeners();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uname'] = this.uname;
    data['mobile'] = this.mobile;
    data['avatar'] = this.avatar;
    return data;
  }

  void remove() {
    uname = null;
    mobile = null;
    avatar = null;
    isLogin = false;
    notifyListeners();
  }
}
